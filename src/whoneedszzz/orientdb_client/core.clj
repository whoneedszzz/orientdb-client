(ns whoneedszzz.orientdb-client.core
  (:require [cheshire.core :as json]
            [clojure.spec.alpha :as s])
  (:import [com.orientechnologies.orient.core.db ODatabasePool
                                                 ODatabaseType
                                                 OrientDB
                                                 OrientDBConfig
                                                 OrientDBConfigBuilder]
           [com.orientechnologies.orient.core.db.document ODatabaseDocumentRemote]
           [com.orientechnologies.orient.core.sql.executor OResult
                                                           OResultSet]))

;; Helper functions
(defn- gen-o-config
  "Generates an OrientDB configuration map"
  [config]
  (-> (OrientDBConfigBuilder.)
      (.fromMap config)
      (.build)))

(defn- gen-result-map
  "Generates a map of the properties of a given OrientDB result"
  [result sort?]
  (let [json-str (.toJSON result)
        res-map (json/parse-string json-str)]
    (if sort?
      (into (sorted-map) res-map)
      res-map)))

;; Public functions
(defn begin
  "Begins an OrientDB transaction"
  [session]
  (.begin session))

(defn close-db
  "Close the database connection"
  [db]
  (.close db)
  (not (.isOpen db)))

(defn close-session
  "Close the database session"
  [session]
  (.close session)
  (.isClosed session))

(defn commit
  "Commits an OrientDB transaction"
  [session]
  (.commit session))

(defn connect
  "Connect to a remote OrientDB instance with required and optional properties
  Note: :root-user & :root-password may be nil, but are required for root tasks"
  [{:keys [url root-user root-password config db-name db-user db-password pool?]}]
  (let [db-config (gen-o-config config)
        db (OrientDB. url root-user root-password db-config)]
    (if pool?
      (let [pool (ODatabasePool. db ^String db-name ^String db-user ^String db-password)]
        {:db db :session (.acquire pool)})
      (let [session (.open db db-name db-user db-password)]
        {:db db :session session}))))

(defn create-db
  "Creates an OrientDB database
  Note: Root credentials must have been provided in connect"
  [{:keys [db db-name db-type config]}]
  (let [db-config (gen-o-config config)]
    (case db-type
      "plocal" (.create db db-name ODatabaseType/PLOCAL db-config)
      "memory" (.create db db-name ODatabaseType/MEMORY db-config))))

(defn drop-db
  "Drops given OrientDB database
  Note: Root credentials must have been provided in connect"
  [{:keys [db db-name]}]
  (.drop db db-name))

(defn gen-odb-map
  "Generates a map with string keys for OrientDB"
  [params]
  (into {}
        (for [[k v] params]
          [(name k) v])))

(defn resultset->maps
  "Converts an OResultSet to a vector of map(s)"
  [result-set sort?]
  (let [map-vec (atom [])]
    (while (.hasNext result-set)
      (let [result (.next result-set)
            result-map (gen-result-map result sort?)]
        (swap! map-vec conj result-map)))
    @map-vec))

(defn rollback
  "Rolls back an OrientDB transaction"
  [session]
  (.rollback session))

(defn script
  "Executes a script"
  [{:keys [session lang script params]}]
  (let [o-params (gen-odb-map params)
        result-set (.execute session lang script o-params)
        results (resultset->maps result-set false)]
    (.close result-set)
    results))

;; Specs
(s/def ::config map?)
(s/def ::db #(instance? OrientDB %))
(s/def ::db-name string?)
(s/def ::db-password string?)
(s/def ::db-type (s/and string? #(re-matches #"plocal|memory" %)))
(s/def ::db-user string?)
(s/def ::lang string?)
(s/def ::orientdb-config #(instance? OrientDBConfig %))
(s/def ::params map?)
(s/def ::pool? boolean?)
(s/def ::result #(instance? OResult %))
(s/def ::result-set #(instance? OResultSet %))
(s/def ::root-password (s/or :s string? :n nil?))
(s/def ::root-user (s/or :s string? :n nil?))
(s/def ::script string?)
(s/def ::session #(instance? ODatabaseDocumentRemote %))
(s/def ::sort? boolean?)
(s/def ::sorted-map (s/and sorted? map?))
(s/def ::url (s/and string? #(re-matches #"(remote|embedded):((\w+(-\w+)*)|\.*)+(\/\w+(-\w+)*)*(\/(\w+(-\w+)*))*" %)))
(s/def ::vec-of-maps (s/coll-of map? :into []))

(s/fdef begin
        :args (s/cat :session ::session)
        :ret ::session)
(s/fdef close-db
        :args (s/cat :db ::db)
        :ret boolean?)
(s/fdef close-session
        :args (s/cat :session ::session)
        :ret boolean?)
(s/fdef commit
        :args (s/cat :session ::session)
        :ret ::session)
(s/fdef connect
        :args (s/cat :args (s/keys :req-un [::url ::root-user ::root-password ::config ::db-name ::db-user ::db-password ::pool?]))
        :ret ::session)
(s/fdef create-db
        :args (s/cat :args (s/keys :req-un [::db ::db-name ::db-type ::config]))
        :ret nil?)
(s/fdef drop-db
        :args (s/cat :args (s/keys :req-un [::db ::db-name])))
(s/fdef gen-o-config
        :args (s/cat :config ::config)
        :ret ::orientdb-config)
(s/fdef gen-odb-map
        :args (s/cat :params ::params)
        :ret ::params)
(s/fdef gen-result-map
        :args (s/cat :result ::result :sort? ::sort?)
        :ret ::sorted-map)
(s/fdef resultset->maps
        :args (s/cat :result-set ::result-set :sort? ::sort?)
        :ret ::vec-of-maps)
(s/fdef rollback
        :args (s/cat :session ::session)
        :ret ::session)
(s/fdef script
        :args (s/cat :args (s/keys :req-un [::session ::lang ::script ::params]))
        :ret ::vec-of-maps)
