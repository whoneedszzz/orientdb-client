## [202011271258]
### Changed
- Updated com.orientechnologies/orientdb-core & com.orientechnologies/orientdb-client to "3.0.35"

## [202005101819]
### Changed
- Updated com.orientechnologies/orientdb-core & com.orientechnologies/orientdb-client to "3.0.31"

## [202005020018]
### Other
- README version numbers were out of date

## [202005020014]
### Removed
- `keywords?` parameter and functionality from `gen-result-map`, `resultset->maps`, and `script` functions as the keys
that start with "@", such as "@class", do not work with keywords

## [202004292011]
### Removed
- Explicit `org.clojure/clojure` dependency in `deps.edn`

## [202004291352]
### Added
- Connect to OrientDB instance with or without connection pooling
- Create database
- Drop database
- Execute script
- Begin, commit, and rollback transactions
- Close session
- Close database
### Changed
- Expand the README to include installation, usage, testing methodology, and issue tracking