# orientdb-client

An OrientDB client library for Clojure containing the base connection functionality for APIs within other namespaces

## Why another library?

There are a few libraries that already add support for connecting to OrientDB in Clojure. So you may be
wondering why create another library. The main goal of this library is to remain as simple as possible 
([Simple, Made Easy](https://www.youtube.com/watch?v=oytL881p-nQ)). In that light, this library only
implements core client connection functionality, transactions, and scripts. 

This library serves as a core and thus to fully implement a client one should also include the API functionality they 
desire. For the SQL API, see [orientdb-client-sql](https://gitlab.com/whoneedszzz/orientdb-client-sql). For the Document
API, see [orientdb-client-doc](https://gitlab.com/whoneedszzz/orientdb-client-doc).

A secondary goal is to always use the latest Java API to maintain support of current OrientDB databases.
A consumer of such a library should not need to seek a new library, fork the library, nor wait unreasonably long 
for the library's maintainers to update the driver simply to continue using the library with a current database 
and latest API. Changes to the API libraries will be monitored, but as we are not infallible, should a 
maintainer miss an update, please don't hesitate to politely create an issue that notifies of the update, and 
it will be quickly updated.

## Installation

Deps
```clojure
whoneedszzz/orientdb-client {:mvn/version "202011271258"}
```

Leiningen
```clojure
[whoneedszzz/orientdb-client "202011271258"]
```

## Usage

#### Require
```clojure
(ns oclient.test
  (:require [whoneedszzz.orientdb-client.core :as oclient]))
```

#### Connect
```clojure
(def conn (oclient/connect {:url "url" 
                            :root-user "root" 
                            :root-password "password" 
                            :config {} 
                            :db-name "test" 
                            :db-user "admin" 
                            :db-password "admin" 
                            :pool? true}))
```
- Returns a map containing both the database and session instances
- To connect without root set `:root-user` and `:root-password` to `nil`
- To connect without a connection pool set `:pool?` to `false`
- To add configuration parameters add parameters to `:config`
  * Note: parameters must match official enum in [OGlobalConfiguration](
  https://github.com/orientechnologies/orientdb/blob/develop/core/src/main/java/com/orientechnologies/orient/core/config/OGlobalConfiguration.java)
- URL can be plocal or mem

#### Create database
```clojure
(oclient/create-db {:db (:db conn) 
                    :db-name "foobar" 
                    :db-type "plocal" 
                    :config {}})
```
- Returns nil (as per OrientDB Java API)
- Note: requires root credentials to be passed when creating the connection previously
- Type may be `plocal` or `mem`
- To add configuration parameters add parameters to `:config`
  * Note: parameters must match official enum in [OGlobalConfiguration](
  https://github.com/orientechnologies/orientdb/blob/develop/core/src/main/java/com/orientechnologies/orient/core/config/OGlobalConfiguration.java)

#### Drop database
```clojure
(oclient/drop-db {:db (:db conn) 
                  :db-name "foobar"})
```
- Returns nil (as per OrientDB Java API)
- Note: requires root credentials to be passed when creating the connection previously

#### Execute script
```clojure
(def script (oclient/script {:session (:session conn) 
                             :lang "sql" 
                             :script "begin; 
                                     let a = create vertex Person set firstName = :fName1, lastName = :lName1; 
                                     let b = create vertex Person set firstName = :fName2, lastName = :lName2; 
                                     commit; 
                                     return [$a, $b];" 
                             :options {:fName1 "John"
                                       :lName1 "Snow" 
                                       :fName2 "Ned" 
                                       :lName2 "Stark"}}))
```
- Returns a vector of map(s) containing the results
- Can use any language supported by OrientDB's script system

#### Execute queries and/or commands with a transaction
```clojure
(let [session (:session conn)]
  (oclient/begin session)
  ;; commands and/or queries
  (try 
    (oclient/commit session)
    (catch Exception e (do
                         (oclient/rollback session)
                         (str "caught exception: " (.getMessage e))))))
```
- Note: commits do not get automatically rolled back to give you the ability to handle the exception the way you would like
- Commits **must** be rolled back to resume normal operation
  * Otherwise, subsequent commands/queries will still be part of the transaction
  
#### Close session

```clojure
(oclient/close-session (:session conn))
```

- With a connection pool this does not actually close the session, but releases the resources back to the pool

#### Close database

```clojure
(oclient/close-db (:db conn))
```

## Testing

Testing consists of using test data with a real OrientDB database in the REPL during development. Previous REPL 
executions were ran again to ensure regressions did not result after new functionality implementations. The possibility
of subtle timing related issues arising caused automated tests to not be utilized. Desiring to discourage over-confidence
in tests contributed as well. Be confident by verifying each function with your own test data! 😊

## Issues

If you discover any issues or if you believe there are any features missing please create an [Issue](
https://gitlab.com/whoneedszzz/orientdb-client/-/issues/new) and please be reasonably detailed to best address your
input. 😊